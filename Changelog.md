# Mein Portfolio-Projekt

Dies ist ein Portfolio-Projekt, das im Rahmen eines Praktikums mit Flutter entwickelt wird. Die App enthält verschiedene Seiten, die Informationen über mich und meine Fähigkeiten darstellen.

## Changelog

### Initiale Einrichtung
- Installation der Entwicklungsumgebung und Einrichtung des Git-Repositories.
- Erstellung einer README-Datei und Initialisierung des Flutter-Projekts.

### Erweiterung der Inhalte und Implementierung des Routings
- **Startseite**: Willkommensgruß, Foto und kurze Einführung.
- **Über mich-Seite**: Name, Status als Student, Interessen, Motivation und LinkedIn-Profil.
- **Fähigkeiten-Seite**: Darstellung von Programmiersprachen, Tools und Soft Skills.
- **Navigation**: Implementierung einer Navigationsleiste (Drawer) zum Wechseln zwischen den Seiten.
- **Zentrierung der Inhalte**: Inhalte auf der `SkillsPage` und `AboutPage` zentriert und als Listen dargestellt.
