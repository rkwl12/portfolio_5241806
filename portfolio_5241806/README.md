# portfolio_5241806

  

Wie bereits im Changelog beschrieben, handelt es sich um ein Projekt, welches ich im Zuge meines Studiums innerhalb eines Praktikums bearbeite.
Diese README bezieht sich auf Änderungen innerhalb des Flutter-Projekts.


## Changelog

#### 06/2024
- main.dart angepasst
- home_page.dart, about_page.dart, skills_page.dart erstellt
- oben genannte Dateien angepasst, gefüllt und bearbeitet
- Screenshots zum überprüfen der Funktionalitäten und Darstellungen hinzugefügt
- README Dateien geupdated



## Hinweis

Diese README Datei ist ebenso wie das Projekt stetig im Aufbau und wird entsprechend erweitert und geändert.
