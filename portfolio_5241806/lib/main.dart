// lib/main.dart
import 'package:flutter/material.dart';
import 'home_page.dart';
import 'about_page.dart';
import 'skills_page.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Mein Portfolio',
      debugShowCheckedModeBanner: false,
      theme: ThemeData.dark(),
      home: HomePage(),
      routes: {
        '/home': (context) => HomePage(),
        '/about': (context) => AboutPage(),
        '/skills': (context) => SkillsPage(),
      },
    );
  }
}