// lib/skills_page.dart
import 'package:flutter/material.dart';

class SkillsPage extends StatelessWidget {
  const SkillsPage({super.key});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Skills'),
      ),
      body: Center(
        child: SizedBox(
          width: 300,
        child: ListView(
          shrinkWrap: true,
          children: const <Widget>[
            ListTile(
              leading: Icon(Icons.code),
              title: Text('Programmiersprachen'),
              subtitle: Text('Dart, Java, SQL'),
            ),
            ListTile(
              leading: Icon(Icons.build),
              title: Text('Tools'),
              subtitle: Text('IntelliJ IDEA, VS Code, GitLab, Flutter'),
            ),
            ListTile(
              leading: Icon(Icons.people),
              title: Text('Soft Skills'),
              subtitle: Text('Teamarbeit, Problemlösung, Kommunikation'),
            ),
          ],
         ),
        ),
      ),
    );
  }
}
