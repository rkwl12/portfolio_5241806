// lib/about_page.dart
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';


class AboutPage extends StatelessWidget {
  const AboutPage({super.key});

  final String linkedInUrl = 'https://www.linkedin.com/in/robinkowalik/';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Über mich'),
      ),
      body: Center(
        child: SizedBox(
        width: 300,
      child: ListView(
        shrinkWrap: true,
        children: <Widget>[
          const ListTile(
            leading: Icon(Icons.person),
            title: Text('Name'),
            subtitle: Text('Robin Kowalik'),
          ),
          const ListTile(
            leading: Icon(Icons.school),
            title: Text('Status'),
            subtitle: Text('Student im Bereich Wirtschaftsinformatik'),
          ),
          const ListTile(
            leading: Icon(Icons.interests),
            title: Text('Interessen'),
            subtitle: Text('Wirtschaft, Technik, Informatik, Automobilbranche'),
          ),
          const ListTile(
            leading: Icon(Icons.auto_graph),
            title: Text('Motivation'),
            subtitle: Text('Wir haben schon zu wenig Zeit alles zu lernen, warum dann noch Zeit verschwenden'),
          ),
          ListTile(
            leading: const Icon(Icons.link),
            title: const Text('LinkedIn'),
            subtitle: const Text('Besuche mich auf LinkedIn'),
            onTap: () async {
              if (await canLaunch(linkedInUrl)) {
                await launch(linkedInUrl);
              } else {
                throw 'Could not launch $linkedInUrl';
                  }
                },
               ),
             ],
           ),
        ),
      ),
    );
  }
}