# Flutter Projekt: portfolio_5241806

## Kontaktdaten

Name:   Robin Kowalik
<br />Kürzel: rkwl12
<br />Mail:   robin.kowalik@mnd.thm.de

## Projektbeschreibung

In diesem Repository befindet sich ein Projekt, das mit dem Flutter Framework entwickelt wird. Es entsteht eine App oder Web-Anwendung, die auf Google Firebase gehostet wird.

## Voraussetzungen auf macOS

- IDE (ich verwende VS Code)
- Homebrew als Paketmanager
- Ruby Version 3 oder neuer
- CocoaPods
- Flutter Framework mit Dart als Sprache
- Android Studio für Android SDK und als Emulator
- Xcode

## Beschreibung der Tools

- **VS Code**: Integrierte Entwicklungsumgebung (IDE) zum Schreiben und Bearbeiten von Code
- **Homebrew**: Paketmanager für macOS zum einfachen Installieren und Aktualisieren von Software
- **Ruby**: Allzweck-Programmiersprache, die für die Entwicklung von Web-Anwendungen verwendet wird
- **CocoaPods**: Abhängigkeitsmanager für Xcode-Projekte, der die Integration von Bibliotheken in iOS-, Mac-, tvOS- und watchOS-Projekte vereinfacht
- **Flutter**: Open-Source-UI-Software-Development-Kit zur Erstellung von nativ kompilierten Anwendungen für mobile, Web- und Desktop-Plattformen aus einer einzigen Codebasis
- **Dart**: Objektorientierte Programmiersprache, die von Google entwickelt wurde und als Sprache für Flutter verwendet wird
- **Android Studio**: Integrierte Entwicklungsumgebung für die Entwicklung von Android-Anwendungen
- **Xcode**: Integrierte Entwicklungsumgebung von Apple für die Entwicklung von Software für macOS, iOS, iPadOS, tvOS und watchOS

## Schlusswort

Diese README sowie das Projekt befinden sich im Aufbau. Neue Informationen werden stets in den Dateien "Screenshots", Changelog und der README des Flutter Projekts aktualisiert.
